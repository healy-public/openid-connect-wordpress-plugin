<?php

namespace Healy\Academy;

//require_once __DIR__ . '/vendor/autoload.php';

function log($msg)  {
    if (is_array($msg) || is_object($msg)) {
        error_log(json_encode($msg));
    } else {
        error_log($msg);
    }
}

add_filter('healy-openid-connect-pre-login-user-update', function (
    \WP_User $user,
    object $claims,
    string $access_token
): \WP_User {
    log('filter: healy-openid-connect-pre-login-user-update');
    return $user;
}, 10, 3);


add_filter('healy-openid-connect-claims-filter', function (object $claims, string $access_token): object {
    log('filter: healy-openid-connect-claims-filter');
    return $claims;
}, 10, 2);

add_filter('healy-openid-connect-user-identifying-claim', function (array $identifying_fields): array {
    log('filter: healy-openid-connect-user-identifying-claim');
    return $identifying_fields;
}, 10, 1);

add_filter('healy-openid-connect-provision-new-user', function (
    ?\WP_User $user,
    object $claims,
    string $access_token
): ?\WP_User {
    log('filter: healy-openid-connect-provision-new-user');
    return $user;
}, 10, 3);

// @deprecated landing page redirection is deprecated and filter won't be called any longer
// add_filter('healy-openid-connect-landing-page', function (?string $landing_page_url): ?string {
//     log('filter: healy-openid-connect-landing-page');
//     return $landing_page_url;
// }, 10, 1);

add_filter('healy-openid-connect-user-update-parameters', function (
    array $user_params,
    object $claims,
    string $access_token
): array {
    log('filter: healy-openid-connect-user-update-parameters');
    return $user_params;
}, 10, 3);

add_filter('healy-openid-connect-user-roles', function (
    array $current_roles,
    \WP_User $user,
    object $claims,
    string $access_token
): array {
    log('filter: healy-openid-connect-user-roles');

    $email = $claims->email ?? '';
    // add employee role to anybody coming in with a healy company email adress.
    if (1 === preg_match('/@healy(world.net|.world)$/', $email)) {
        $current_roles[] = 'editor';
        log("healy-openid-connect-user-roles: adding 'editor' role for email: '{$email}'");
    }

    return $current_roles;
}, 10, 4);

add_filter('healy-openid-connect-user-capabilities', function (
    array $current_caps,
    \WP_User $user,
    object $claims,
    string $access_token
): array {
    log('filter: healy-openid-connect-user-capabilities');

    $email = $claims->email ?? '';
    // add employee capability to anybody coming in with a healy company email adress.
    if (1 === preg_match('/@healy(world.net|.world)$/', $email)) {
        $current_caps[] = 'employee';
        log("healy-openid-connect-user-capabilities: adding 'employee' capability for email: '{$email}'");
    }

    return $current_caps;
}, 10, 4);
