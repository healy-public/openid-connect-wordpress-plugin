#!/usr/bin/env sh

if [ $# != 2 ]; then
    echo "usage: $0 <dir1> <dir2>"
    exit
fi
dir1=$1
dir2=$2

echo "${dir1} <-> ${dir2}"

while true; do
    #clear
    #echo unison -batch -auto  $dir1 $dir2 -ignore 'Path vendor' -ignore 'Path .*.sw*'
    IGNORE="-ignore 'Path vendor'"
    IGNORE="${IGNORE} -ignore 'Path .trash'"
    IGNORE="${IGNORE} -ignore 'Path composer.*'"
    IGNORE="${IGNORE} -ignore 'Path tests'"
    IGNORE="${IGNORE} -ignore 'Path .*.sw*'"
    IGNORE="${IGNORE} -ignore 'Path .git'"
    IGNORE="${IGNORE} -ignore 'Path x.*'"
    IGNORE="${IGNORE} -ignore 'Path file-watcher.sh'"
    IGNORE="${IGNORE} -ignore 'Path sync-dirs.sh'"
    IGNORE="${IGNORE} -ignore 'Path Session.vim'"
    IGNORE="${IGNORE} -ignore 'Path .gitignore'"
    echo unison -batch -auto  $dir1 $dir2 ${IGNORE}
    unison -batch -auto  $dir1 $dir2   \
        -ignore 'Path vendor'          \
        -ignore 'Path .trash'          \
        -ignore 'Path composer.lock'   \
        -ignore 'Path tests'           \
        -ignore 'Path .*.sw*'          \
        -ignore 'Path src/.*.sw*'      \
        -ignore 'Path .git'            \
        -ignore 'Path x.*'             \
        -ignore 'Path *.x'             \
        -ignore 'Path file-watcher.sh' \
        -ignore 'Path sync-dirs.sh'    \
        -ignore 'Path Session.vim'     \
        -ignore 'Path .gitignore'
    sleep 1
done
