<?php

declare(strict_types=1);

namespace Healy\OpenIdConnect\Plugin;

// This file tries to overload global wordpress function inside the namespace
// of this plugin. That way we can run this test without having a wordpress
// context or database running. Of course this is limited approach but helps
// separating plugin business logic from the intrinsics of the WordPress core.

use function Functional\tail;

function scratch_op(callable $f)
{
    static $scratch = [];
    return $f($scratch);
}
function scratch_clear(): void
{
    scratch_op(fn (&$db) => $db = []);
}

function scratch(): array
{
    return scratch_op(fn ($db) => $db);
}
function scratch_set($key, $val): void
{
    scratch_op(fn(&$db) => $db[$key] = $val);
}
function scratch_get($key, $default = null) /* :mixed */
{
    return scratch_op(fn($db) => $db[$key] ?? $default);
}

function get_option(string $key, $default = null) /* : mixed  */
{
    return scratch_get($key, $default);
    //return Test\test_options()[$key] ?? $default ?? false;
}

function update_option(string $key, $value, $autoload = null): bool
{
    scratch_set($key, $value);
    return true;
}

function apply_filters(): array // string $filter_name, array $roles, object $claims, string $access_token): array
{
    $args = func_get_args();

    // is a filter defined at all?
    if (empty(Test\FunctionsTest::$filter)) {
        return $args[1];
    }

    return (Test\FunctionsTest::$filter)([
        'filter_name'   => $args[0],    // filter_name,
        'args'          => tail($args)  // [$roles, $claims, $access_token]
    ]);
    //return (Test\FunctionsTest::$filter)($filter_name, $roles, $claims, $access_token);
}


function get_user_meta(int $user_id, string $key = '', bool $single = false) /* : array */
{
    $user_meta = scratch_get('user-meta', [])[$user_id] ?? [];
    return empty($key) ? $user_meta : ($user_meta[$key] ?? []);
}

// overloading wordpress function: store meta data in test class scratch instead
function update_user_meta(int $user_id, string $key, $val, $prev_value = ''): int
{
    $meta = scratch_get('user-meta', []);
    (empty($meta[$user_id]) && ($meta[$user_id] = []));
    $meta[$user_id][$key] = $val;
    scratch_set('user-meta', $meta);
    return 17;

    $meta = Test\FunctionsTest::$scratch['meta'] ?? [];
    (empty($meta[$user_id]) && ($meta[$user_id] = []));
    $meta[$user_id][$key] = $val;
    Test\FunctionsTest::$scratch['meta'] = $meta;
    return 17;
}
