<?php

declare(strict_types=1);

namespace Healy\OpenIdConnect\Plugin\Test;

require_once __DIR__ . '../../wordpress-mocking.php';

use function Healy\OpenIdConnect\Plugin\scratch;
use function Healy\OpenIdConnect\Plugin\scratch_op;
use function Healy\OpenIdConnect\Plugin\scratch_get;
use function Healy\OpenIdConnect\Plugin\scratch_set;
use function Healy\OpenIdConnect\Plugin\scratch_clear;

final class ScratchTest extends PluginTestCase
{
    public function test_scratch_get_set(): void
    {
        $this->assertEquals([], scratch());

        $key = 'a';
        $this->assertNull(scratch_get($key));
        scratch_set($key, 12);
        $this->assertEquals(12, scratch_get($key));

        scratch_set('b', ['c' => 3]);
        $this->assertEquals(['c' => 3], scratch_get('b'));
    }

    public function test_scratch_clear(): void
    {
        scratch_set('b', ['c' => 3]);
        $this->assertEquals(['c' => 3], scratch_get('b'));

        scratch_clear();
        $this->assertEquals([], scratch());
    }
}
