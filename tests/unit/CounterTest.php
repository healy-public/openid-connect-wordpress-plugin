<?php

declare(strict_types=1);

namespace Healy\OpenIdConnect\Plugin\Test;

use Healy\OpenIdConnect\Plugin\Counter;
use Healy\OpenIdConnect\Plugin\Test\PluginTestCase;

use function Healy\OpenIdConnect\Plugin\scratch;
use function Healy\OpenIdConnect\Plugin\scratch_op;
use function Healy\OpenIdConnect\Plugin\scratch_get;
use function Healy\OpenIdConnect\Plugin\scratch_set;
use function Healy\OpenIdConnect\Plugin\scratch_clear;
use function Healy\OpenIdConnect\Plugin\get_option;

final class CounterTest extends PluginTestCase
{
    public function setUp(): void
    {
        scratch_clear();
    }

    public function test_can_be_constructed(): void
    {
        $this->assertInstanceOf(Counter::class, new Counter());
    }

    public function test_all(): void
    {
        $this->assertIsArray((new Counter())->all());
    }

    public function test_get_set_get(): void
    {
        $m = new Counter();

        $this->assertEquals(0, $m->get('some-key'));
        $m->set('some-key', 23);
        $this->assertEquals(23, $m->get('some-key'));
    }

    public function test_inc_dec(): void
    {
        $m = new Counter();
        $this->assertEquals(0, $m->get('some-key'));

        $m->set('some-key', 23);
        $m->inc('some-key');
        $this->assertEquals(24, $m->get('some-key'));
    }

    public function test_inc_inits_with_one(): void
    {
        $m = new Counter();
        $this->assertEquals(1, $m->inc('some-non-existing-key')->get('some-non-existing-key'));
    }
}
