<?php

declare(strict_types=1);

namespace Healy\OpenIdConnect\Plugin\Test;

use Mockery as m;

use Healy\OpenIdConnect\Plugin\Error;
use Healy\OpenIdConnect\Plugin\Test\PluginTestCase;

use function Healy\OpenIdConnect\Plugin\{
    blocking_reasons,
    check_blocking_claims,
    check_mandatory_claims,
    is_untouchable,
    oidc_options,
    parse_multi_line_claims_format,
    untouchable_roles,
    update_user_capabilities,
    update_user_meta_from_claims,
    update_user_roles,
};

use function Healy\OpenIdConnect\Plugin\scratch;
use function Healy\OpenIdConnect\Plugin\scratch_op;
use function Healy\OpenIdConnect\Plugin\scratch_get;
use function Healy\OpenIdConnect\Plugin\scratch_set;
use function Healy\OpenIdConnect\Plugin\scratch_clear;
use function Healy\OpenIdConnect\Plugin\get_option;

use function Functional\contains;
use function Functional\matching as mop;

function test_options_(array $update = null, bool $flush = false): array
{
    static $cache = [];
    if ($flush) {
        $cache = [];
    }
    if (!empty($update)) {
        list($key, $val) = $update;
        $cache[$key] = $val;
        oidc_options(true);
    }
    return ['healy-openid-connect' => $cache];
}
function test_options(array $update = null, bool $flush = false): array
{
    if ($flush) {
        scratch_clear();
    }

    if (!empty($update)) {
        list($key, $val) = $update;
        $options = scratch_get('healy-openid-connect', []);
        $options[$key] = $val;
        scratch_set('healy-openid-connect', $options);
        oidc_options(true);
    }
    return scratch(); // ['healy-openid-connect' => scratch()];
}

// helper function to define functional matching of functions by their call
// parameters. Stored to the $filters var which is used in apply_filters when
// overloading the global wordpress apply_filter function.
//
// with mock_filter a test method can overload a specific filter, for a specific set up params, e.g.:
//
//      mock_filter('filter-name', fn(int $i, string $s) => $i + 1);
//
//  would overload:
//
//      apply_filter('filter-name', 17, 'foo);
//           array $current_roles,
//           \WP_User $user,
//           object $claims,
//           string $access_token
//       ): array {
//           return $claims->roles;
//       });
function mock_filter(string $filter_name, callable $filter_func)
{
    FunctionsTest::$filter = mop([
        [
            fn(array $ctx) => $ctx['filter_name'] === $filter_name,
            fn(array $ctx) => $filter_func(...$ctx['args'])
        ]
    ]);
}

final class FunctionsTest extends PluginTestCase
{
    public static $filter = null;
    public static $scratch = null;

    public function setUp(): void
    {
        self::$scratch = [];
    }

    public function tearDown(): void
    {
        self::$scratch = null;
        self::$filter = null;
        test_options(null, true);
    }

    public function testMatch(): void
    {
        $f = mop([
            [
                fn(array $ctx) => $ctx['key'] === 'foo',
                fn(array $ctx) => $ctx['val']
            ]
        ]);

        $this->assertEquals('bar', $f(['key' => 'foo', 'val' => 'bar']));
    }

    public function test_test_options(): void
    {
        $this->assertEmpty(get_option('healy-openid-connect'));
        test_options(['k', 'v']);
        $this->assertEquals(['k' => 'v'], get_option('healy-openid-connect'));
    }

    public function testUpdateUserRolesForUntouchableUser(): void
    {
        $wp_user = m::mock(\WP_User::class)->makePartial();
        $wp_user->roles = ['administrator', 'foo', 'bar'];
        test_options(['untouchable-roles', 'administrator']);

        mock_filter('healy-openid-connect-user-roles', function (
            array $current_roles,
            \WP_User $user,
            object $claims,
            string $access_token
        ): array {
            return $claims->roles;
        });

        $wp_user->shouldReceive('remove_role')->withArgs(function (string $role) use ($wp_user) {
            $wp_user->roles = array_diff($wp_user->roles, [$role]);
            return true;
        });

        $wp_user->shouldReceive('add_role')->withArgs(function (string $role) use ($wp_user) {
            if (!contains($wp_user->roles, $role)) {
                $wp_user->roles[] = $role;
            }
            return true;
        });

        update_user_roles($wp_user, (object) ['roles' => ['123', 'hieopai']], "access_token");
        $this->assertEquals(['administrator', 'foo', 'bar'], $wp_user->roles);
    }

    public function testUpdateUserRolesWithProtection(): void
    {
        $wp_user = m::mock(\WP_User::class)->makePartial();
        $wp_user->roles = ['administrator'];
        test_options(['protected-roles', 'administrator']);

        //self::$filter = fn() => ['foo', 'bar', 'xxx'];
        mock_filter('healy-openid-connect-user-roles', function (
            array $current_roles,
            \WP_User $user,
            object $claims,
            string $access_token
        ): array {
            return $claims->roles;
        });

        $wp_user->shouldReceive('remove_role')->withArgs(function (string $role) use ($wp_user) {
            $wp_user->roles = array_diff($wp_user->roles, [$role]);
            return true;
        });

        $wp_user->shouldReceive('add_role')->withArgs(function (string $role) use ($wp_user) {
            if (!contains($wp_user->roles, $role)) {
                $wp_user->roles[] = $role;
            }
            return true;
        });

        update_user_roles($wp_user, (object) ['roles' => ['foo']], "access_token");
        $this->assertEquals(['administrator', 'foo'], $wp_user->roles);
    }

    public function testUpdateUserRoles(): void
    {
        $wp_user = m::mock(\WP_User::class)->makePartial();
        $wp_user->roles = ['subscriber'];

        //self::$filter = fn() => ['foo', 'bar', 'xxx'];
        mock_filter('healy-openid-connect-user-roles', function (
            array $current_roles,
            \WP_User $user,
            object $claims,
            string $access_token
        ): array {
            return $claims->roles;
        });

        $wp_user->shouldReceive('remove_role')->withArgs(function (string $role) use ($wp_user) {
            $wp_user->roles = array_diff($wp_user->roles, [$role]);
            return true;
        });

        $wp_user->shouldReceive('add_role')->withArgs(function (string $role) use ($wp_user) {
            if (!contains($wp_user->roles, $role)) {
                $wp_user->roles[] = $role;
            }
            return true;
        });

        update_user_roles($wp_user, (object) ['roles' => ['foo', 'bar', 'xxx']], "access_token");
        $this->assertEquals(['foo', 'bar', 'xxx'], $wp_user->roles);
    }

    // apply_filters('healy-openid-connect-user-capabilities', $current_caps, $user, $claims, $access_token)
    public function testUpdateUserCapabilities(): void
    {
        $wp_user = m::mock(\WP_User::class)->makePartial();
        $wp_user->allcaps = ['a' => true, 'b' => true];

        mock_filter('healy-openid-connect-user-capabilities', function (
            array $current_caps,
            \WP_User $user,
            object $claims,
            string $access_token
        ): array {
            return ['b', 'c'];
        });

        $wp_user->shouldReceive('remove_cap')->withArgs(function (string $capabilitiy) use ($wp_user) {
            unset($wp_user->allcaps[$capabilitiy]);
            return true;
        });

        $wp_user->shouldReceive('add_cap')->withArgs(function (string $capabilitiy) use ($wp_user) {
            $wp_user->allcaps[$capabilitiy] = true;
            return true;
        });

        update_user_capabilities($wp_user, (object)[], "access_token");
        $this->assertEquals(['b' => true, 'c' => true], $wp_user->allcaps);
    }

    public function testIsUntouchable(): void
    {
        test_options(['untouchable-roles', 'administrator']);

        $wp_user = m::mock(\WP_User::class);
        $wp_user->roles = ['subscriber'];
        $this->assertFalse(is_untouchable($wp_user));

        $wp_user->roles = ['administrator'];
        $this->assertTrue(is_untouchable($wp_user));
    }

    public function testUntouchableRoles(): void
    {
        test_options(['untouchable-roles', 'foo,bar,xxx']);
        $this->assertEquals(['foo', 'bar', 'xxx'], untouchable_roles());

        test_options(['untouchable-roles', 'foo, bar   ']);
        $this->assertEquals(['foo', 'bar'], untouchable_roles());

        test_options(['untouchable-roles', '']);
        $this->assertEquals([], untouchable_roles());

        test_options(['untouchable-roles', 'foo']);
        $this->assertEquals(['foo'], untouchable_roles());

        test_options(['untouchable-roles', '    foo      ']);
        $this->assertEquals(['foo'], untouchable_roles());

        test_options(['untouchable-roles', 'foo,   bar-laber  gonzo!; x ']);
        $this->assertEquals(['foo', 'bar-laber', 'gonzo!', 'x'], untouchable_roles());
    }

    public function testUpdateUserMeta(): void
    {
        $wp_user = m::mock(\WP_User::class)->makePartial();

        // mock the filter to return all claims as user meta data
        $filter_func = fn (
            array $user_meta_entries, \WP_User $user, object $claims, string $access_token
        ): array
            => (array) $claims;
        mock_filter('healy-openid-connect-user-meta', $filter_func);

        $wp_user->ID = $wp_user_id = 23;
        $claims = ['a' => 1, 'b' => 2];
        update_user_meta_from_claims($wp_user, (object) $claims, [], "access_token");

        // this proves the return (key, value) list from the filter is added updated into the user meta
        $this->assertEquals($claims, scratch_get('user-meta', [])[$wp_user_id] ?? []);
        //$this->assertEquals($claims, self::$scratch['meta'][$wp_user->ID] ?? []);

        $newclaims = [          'b' => 3, 'c' => 4];
        update_user_meta_from_claims($wp_user, (object) $newclaims, [], "access_token");
        // this proves the existing meta are update, and old ones are not overwritten
        $this->assertEquals(array_merge($claims, $newclaims), scratch_get('user-meta')[$wp_user_id] ?? []);
        //$this->assertEquals(array_merge($claims, $newclaims), self::$scratch['meta'][$wp_user->ID] ?? []);
    }

    public function test_parse_multi_line_claims_format(): void
    {
        $this->assertEquals(
            [
                'line' => 'with "quoted" string!',
                'url' => "with http://service.com",
                'sql' => "select 1 from users;",
                'html' => "<em>xxx</em>",
                'escaped string' => '\"foo\"'
            ],
            parse_multi_line_claims_format(
                "line: with \"quoted\" string!\r\nurl: with http://service.com\r\nsql: select 1 from users;\r\nhtml: <em>xxx</em>\r\nescaped string: \\\"foo\\\"\r\n"
            )
        );
    }

    public function test_parse_multi_line_claims_format_from_nowdoc(): void
    {
        $this->assertEquals(
            [
                'line' => 'with "quoted" string!',
                'url' => "with http://service.com",
                'sql' => "select 1 from users;",
                'html' => "<em>xxx</em>",
                'escaped string' => '\"foo\"'
            ],
            parse_multi_line_claims_format(<<<EOT
                line: with "quoted" string!
                url: with http://service.com
                sql: select 1 from users;
                html: <em>xxx</em>
                escaped string: \\"foo\\"
                EOT)
        );
    }

    public function test_parse_multi_line_claims_format_with_default_right_side(): void
    {
        $this->assertEquals(
            ['a' => 'a', 'b' => 'b', 'c' => 'c'],
            parse_multi_line_claims_format("a:\r\nb:\r\nc\r\n")
        );
        $this->assertEquals(['a' => 'a', 'b' => 'b', 'c' => 'c'], parse_multi_line_claims_format(<<<EOT
            a:
            b
            c:
            EOT
        ));
    }

    public function test_parse_multi_line_claims_format_keys_are_normalized_to_lowerkey(): void
    {
        $this->assertEquals(
            ['a' => 'A', 'b' => 'B', 'c' => 'C'],
            parse_multi_line_claims_format("A:\r\nB:\r\nC\r\n")
        );

        // only one 'a' key. 'a' and 'A' are mapped to the same entry, last one rules: 'a' => 'A'
        $this->assertEquals(['a' => 'X'], parse_multi_line_claims_format("a:x\r\nA:X"));
    }

    public function testUpdateUserMetaFromClaimsMapping(): void
    {
        //test_options(['claims-mapping', "A: a\r\nB: b\r\nD: d"]);
        $mapping = ['A' => 'a', 'B' => 'b', 'D' => 'd'];
        $wp_user = m::mock(\WP_User::class)->makePartial();

        $wp_user->ID = $wp_user_id = 23;
        $claims = ['a' => 1, 'b' => 2, 'c' => 3];
        update_user_meta_from_claims($wp_user, (object) $claims, $mapping, "access_token");

        // this proves the return (key, value) list from the filter is added updated into the user meta
        $this->assertEquals(['A' => 1, 'B' => 2], scratch_get('user-meta')[$wp_user_id] ?? []);
        //$this->assertEquals(['A' => 1, 'B' => 2], self::$scratch['meta'][$wp_user->ID] ?? []);
    }

    public function testUpdateUserMetaFromClaimsMappingAndFalseClaimsValues(): void
    {
        //test_options(['claims-mapping', "A: a"]);
        $mapping = ['A' => 'a'];
        $wp_user = m::mock(\WP_User::class)->makePartial();

        $wp_user->ID = $wp_user_id = 23;
        $claims = ['a' => false];
        update_user_meta_from_claims($wp_user, (object) $claims, $mapping, "access_token");

        // this proves the return (key, value) list from the filter is added updated into the user meta
        $this->assertEquals(['A' => false], scratch_get('user-meta')[$wp_user_id] ?? []);
        //$this->assertEquals(['A' => false], self::$scratch['meta'][$wp_user->ID] ?? []);
    }

    public function test_check_blocking_claims(): void
    {
        // blocking russia
        test_options(['blocking-claims', "country: ru, by"]);

        // ukrania is not blocked
        $user_claims = ['a' => 1, 'country' => 'ua', 'c' => 3];
        $this->assertEquals([], check_blocking_claims($user_claims));

        // country claim is found as blocking
        $user_claims = ['a' => 1, 'country' => 'ru', 'c' => 3];
        $this->assertEquals(['country' => 'ru'], check_blocking_claims($user_claims));
    }

    public function test_check_blocking_claims_key_is_case_insensitive(): void
    {
        // blocking russia
        test_options(['blocking-claims', "cOuNtRy: ru, by"]);

        // user claims is in different case than the blocking claim
        $user_claims = ['CoUnTrY' => 'ru'];

        // the resulting blocking key is the normalized(lowerkey) version of the options key
        $this->assertEquals(['country' => 'ru'], check_blocking_claims($user_claims));
    }

    public function test_claims_blocking_is_not_case_sensitive(): void
    {
        // blocking russia
        test_options(['blocking-claims', "country: ru, by"]);

        // capital 'RU' user country blocked by lowercase 'ru' blocking claim
        $user_claims = ['a' => 1, 'country' => 'RU', 'c' => 3];
        $this->assertEquals(['country' => 'RU'], check_blocking_claims($user_claims));
    }

    public function test_check_blocking_claims_with_multiple_blocks(): void
    {
        // blocking russia
        test_options(['blocking-claims', "country: ru, by\r\nc: 3"]);

        // country claim is found as blocking
        $user_claims = ['a' => 1, 'country' => 'ru', 'c' => "3"];
        $this->assertEquals(['country' => 'ru', 'c' => 3], check_blocking_claims($user_claims));
    }

    public function test_check_mandatory_claims_okay(): void
    {
        // enforce down under origin
        test_options(['mandatory-claims', "country: au, nz"]);

        // australia is okay
        $user_claims = ['a' => 1, 'country' => 'au', 'c' => 3];
        $this->assertEquals([], check_mandatory_claims($user_claims));
    }

    public function test_check_mandatory_claims_okay_is_case_insensitive(): void
    {
        // enforce down under origin
        test_options(['mandatory-claims', "country: au, nz"]);

        // uppercase value is okay
        $user_claims = ['a' => 1, 'country' => 'AU', 'c' => 3];
        $this->assertEquals([], check_mandatory_claims($user_claims));

        // capitalized key is okay
        $user_claims = ['a' => 1, 'Country' => 'au', 'c' => 3];
        $this->assertEquals([], check_mandatory_claims($user_claims));
    }

    public function test_check_mandatory_claims_missing(): void
    {
        // blocking russia
        test_options(['mandatory-claims', "country: au, nz"]);

        // country claim is found missing completely
        $user_claims = ['a' => 1, 'c' => 3];
        $this->assertEquals(['country' => 'au, nz'], check_mandatory_claims($user_claims));
    }

    public function test_check_mandatory_claims_has_wrong_value(): void
    {
        // blocking russia
        test_options(['mandatory-claims', "country: au, nz"]);

        // country claim is found but has the wrong value
        $user_claims = ['a' => 1, 'country' => 'ru', 'c' => 3];
        $this->assertEquals(['country' => 'au, nz'], check_mandatory_claims($user_claims));
    }

    public function test_is_user_blocked_returns_array(): void
    {
        // blocking russia
        test_options(['mandatory-claims', "country: au, nz"]);

        // country claim is found but has the wrong value
        $user_claims = ['a' => 1, 'country' => 'ru', 'c' => 3];
        $this->assertIsArray(blocking_reasons($user_claims));
    }

    public function test_is_user_blocked_returns_reasons(): void
    {
        // blocking russia
        test_options(['mandatory-claims', "country: au, nz"]);

        // country claim is found but has the wrong value
        $user_claims = ['a' => 1, 'country' => 'ru', 'c' => 3];
        $reasons = blocking_reasons($user_claims);
        $this->assertEquals(1, count($reasons));
        $reason = $reasons[0];
        $this->assertInstanceOf(Error::class, $reason);

        //$this->assertEquals(Error::MESSAGE[Error::MISSING_CLAIMS], $reason->text());
        //$this->assertMatchesRegularExpression('/' . Error::MESSAGE[Error::MISSING_CLAIMS] . '$/', (string) $reason);
        $this->assertMatchesRegularExpression('/[E' . Error::MISSING_CLAIMS . ']/', (string) $reason);

        $this->assertEquals(Error::MISSING_CLAIMS, $reason->code());
        $this->assertEquals(['claims' => ['country' => 'au, nz']], $reason->params());
    }
}
