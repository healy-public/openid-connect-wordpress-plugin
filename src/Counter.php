<?php

declare(strict_types=1);

namespace Healy\OpenIdConnect\Plugin;

final class Counter
{
    private array $values = [];

    public function __construct(array $values = [])
    {
        $this->values = $values;
    }

    public function all(): array
    {
        return $this->values;
    }

    // counter allways init with 0
    public function get(string $key): int
    {
        return $this->values[$key] ?? 0;
    }

    public function set(string $key, int $val): Counter
    {
        $this->values[$key] = $val;
        return $this;
    }

    public function inc(string $key): Counter
    {
        $this->values[$key] = $this->get($key) + 1;
        return $this;
    }

    public function dec(string $key): Counter
    {
        $this->values[$key] = $this->get($key) - 1;
        return $this;
    }
}
