<?php

declare(strict_types=1);

namespace Healy\OpenIdConnect\Plugin;

class Cookies
{
    /**
     * Sets a cookie and also updates the $_COOKIE array.
     */
    public static function set(string $name, string $value, int $expire = 0): bool
    {
        setcookie($name, $value, $expire, '/', self::cookie_domain(), is_ssl());
        $_COOKIE[$name] = $value;
        return true;
    }

    public static function get(string $name): ?string
    {
        return isset($_COOKIE[$name]) ? sanitize_text_field($_COOKIE[$name]) : null;
    }

    public static function clear(string $name): void
    {
        if (isset($_COOKIE[$name])) {
            setcookie($name, '', time() - 60 * 60 /* HOUR_IN_SECONDS */, '', '', is_ssl());
            unset($_COOKIE[$name]);
        }
    }

    // logic lifted from: wp-content/plugins/sitepress-multilingual-cms/classes/class-wpml-browser-redirect.php
    private static function cookie_domain(): string
    {
        // fallback to host setting...
        $http_host = ($_SERVER['HTTP_HOST'] ?? 'localhost');
        $http_host = ($http_host === 'localhost') ? '' : $http_host;

        // ...when COOKIE_DOMAIN is not defined
        $domain = (defined('COOKIE_DOMAIN') && COOKIE_DOMAIN) ? COOKIE_DOMAIN : $http_host;
        error_log(print_r("COOKIE_DOMAIN: '{$domain}'", true));

        return $domain;
    }
}
