<?php

declare(strict_types=1);

namespace Healy\OpenIdConnect\Plugin;

use function Functional\{each, map};

class Error
{
    private int $code;
    private string $text;
    private array $params;

    const BLOCKED_BY_CLAIMS                     = 1;
    const MISSING_CLAIMS                        = 2;
    const TOKEN_SIGNATURE_VERIFICATION_FAILED   = 3;
    const INVALID_JWT_TOKEN                     = 4;
    const TOKEN_VALIDATION_FAILED               = 5;
    const CRSF_STATE_CHECK_FAILED               = 6;
    const AUTO_REGISTER_NOT_ENABLED             = 7;
    const WORDPRESS_INSERT_USER_ERROR           = 8;
    const NUKED_BY_PRE_LOGIN_FILTER             = 9;
    const ID_TOKEN_MUST_NOT_BE_NULL             = 10;
    const REST_API_LOGON_EXCEPTION              = 11;

    const MESSAGE = [
        self::BLOCKED_BY_CLAIMS => 'Login blocked by profile fields: {{claims}}',
        self::MISSING_CLAIMS => 'Blocked because of missing mandatory profile fields: {{claims}}',
        self::TOKEN_SIGNATURE_VERIFICATION_FAILED => 'ID_Token Signature Verification Failed: {{token}}',
        self::INVALID_JWT_TOKEN => 'Invalid JWT token: "{{token}}"',
        self::TOKEN_VALIDATION_FAILED => 'Token Validation Failed: {{message}} -- {{token}}',
        self::CRSF_STATE_CHECK_FAILED => 'OIDC logon CRSF state check failed',
        self::AUTO_REGISTER_NOT_ENABLED => 'Auto-registering new users is not enabled', // OIDC WordPress User not yet existing and none created by filter',
        self::WORDPRESS_INSERT_USER_ERROR => 'Internal WordPress error: {{msg}}',
        self::NUKED_BY_PRE_LOGIN_FILTER => 'Username ({{user_login}}) got nuked by OpenId Connect pre-login filter',
        self::ID_TOKEN_MUST_NOT_BE_NULL => 'NULL ID_TOKEN received. (invalid)',
        self::REST_API_LOGON_EXCEPTION => '{{ex}}: {{msg}}',
    ];

    public function __construct(int $code, array $params = [])
    {
        $this->code = $code;
        $this->params = $params;
    }

    public function code(): int
    {
        return $this->code;
    }

    public function params(): array
    {
        return $this->params;
    }

    public function __toString(): string
    {
        // key --> {{key}}
        $keys = map(array_keys($this->params), fn ($key) => "{{{$key}}}");

        // stringified values
        $values = map(
            array_values($this->params),
            fn ($val) => ((is_array($val) || is_object($val)) ? json_encode($val) : $val)
        );

        // "...{{key}}..." --> "...$value..."
        return str_replace($keys, $values, $this->text());
    }

    private function text(): string
    {
        $msg = self::MESSAGE[$this->code];

        // translate message when wordpress translate function is present. This
        // check is to not crash this code when running in test context without
        // wordpress
        if (function_exists('__')) {
            $msg = \__($msg);
        }

        return "[E{$this->code}] {$msg}";
    }
}
